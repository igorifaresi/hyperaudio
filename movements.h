#define BEAT_INIT        0
#define BEAT             16
#define BEAT_HALF        8
#define BEAT_QUARTER     4
#define BEAT_OCTAVE      2
#define BEAT_HALT_OCTAVE 1

#define ON_ENTRY(step, entry) if ((step) == (entry))

#define FOREACH_CICLE(step, entry, cicle) if ((step) > (entry) && ((step) -  (entry))%(cicle) == 0)

#define FOREACH_CICLE_INCLUSIVE(step, entry, cicle) if ((step) >= (entry) && ((step) -  (entry))%(cicle) == 0)

#define START_MOVEMENT_IN(step, beat_part, init_in) if ((step) == 0 && (beat_part) == (init_in))

#define BASIC_SETUP_CHANNEL_FROM_FILE(ctx, channel_number, file_name) ({                   \
    create_music_cursor_from_file(file_name, &ctx->channels[channel_number].cursor);       \
    ctx->channels[channel_number].volum = 1.0;                                             \
    for (u32 i = 0; i < 8; i += 1) {                                                       \
        ctx->channels[channel_number].panning_vec[i] = 1.0;                                \
    }                                                                                      \
    ctx->channels[channel_number].active = true;                                           \
})

#define RELOAD_CHANNEL(ctx, channel_number) ({                                             \
    ctx->channels[channel_number].cursor.reset(&ctx->channels[channel_number].cursor);     \
    ctx->channels[channel_number].active = true;                                           \
})

#define FREE_CHANNEL(ctx, channel_number) ({                                               \
    ctx->channels[channel_number].cursor.free(&ctx->channels[channel_number].cursor);      \
})

static MusicMovementStatus
_beat1_mov(u32 beat_part, u32 step, MusicContext *ctx)
{
    printf("step = %d   ", step);

    // init on init of beat
    if (step == 0 && beat_part != BEAT_INIT) return SKIP;

    // main kick
    START_MOVEMENT_IN(step, beat_part, BEAT_INIT)
        BASIC_SETUP_CHANNEL_FROM_FILE(ctx, 0, "kick.wav");

    FOREACH_CICLE(step, 0, BEAT_HALF)
        RELOAD_CHANNEL(ctx, 0);

    // main snare
    const u32 snare_entry = BEAT*2;
    const u32 snare_cicle = BEAT*2;

    ON_ENTRY(step, snare_entry)
        BASIC_SETUP_CHANNEL_FROM_FILE(ctx, 1, "snare.wav");

    FOREACH_CICLE(step, snare_entry, snare_cicle)
        RELOAD_CHANNEL(ctx, 1);

    // double snare effect
    const u32 double_snare_entry = snare_entry + snare_cicle*2 + BEAT_QUARTER;

    ON_ENTRY(step, double_snare_entry)
        RELOAD_CHANNEL(ctx, 1);

    FOREACH_CICLE(step, double_snare_entry, snare_cicle*4)
        RELOAD_CHANNEL(ctx, 1);

    return RUNNING;
}

static MusicMovementStatus
_awake1_mov(u32 beat_part, u32 step, MusicContext *ctx)
{
    printf("step = %d   ", step);

    if (step == 0 && beat_part != 0) return SKIP;

    START_MOVEMENT_IN(step, beat_part, BEAT_INIT)
        BASIC_SETUP_CHANNEL_FROM_FILE(ctx, 2, "awake1.pt1.wav");

    const u32 block_size = BEAT*2;
    const u32 cicle = block_size*4; 

    FOREACH_CICLE(step, 0, cicle) {
        FREE_CHANNEL(ctx, 2);
        BASIC_SETUP_CHANNEL_FROM_FILE(ctx, 2, "awake1.pt1.wav");
    } 

    FOREACH_CICLE_INCLUSIVE(step, block_size*2, cicle) {
        FREE_CHANNEL(ctx, 2);
        BASIC_SETUP_CHANNEL_FROM_FILE(ctx, 2, "awake1.pt2.wav");
    }

    FOREACH_CICLE_INCLUSIVE(step, block_size*3, cicle) {
        FREE_CHANNEL(ctx, 2);
        BASIC_SETUP_CHANNEL_FROM_FILE(ctx, 2, "awake1.pt3.wav");
    }

    return RUNNING;
}

static void
regist_all_music_movements()
{
    regist_music_movement(0, _beat1_mov);
    regist_music_movement(1, _awake1_mov);
}