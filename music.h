//-----------------------------------------------------------------------------
// Music Source System Functions
//-----------------------------------------------------------------------------

u32
read_music_cursor_from_file(MusicSourceCursor *cursor, f32 *output, u32 qnt)
{
    return (u32)ma_decoder_read_pcm_frames(cursor->generator, output, qnt);
}

void
free_music_cursor_from_file(MusicSourceCursor *cursor)
{
    ma_decoder_uninit(cursor->generator);
    ha_free(cursor->generator);
}

void
reset_music_cursor_from_file(MusicSourceCursor *cursor)
{
    ma_decoder_seek_to_pcm_frame(cursor->generator, 0);
}

void
create_music_cursor_from_file(const u8 *file_name, MusicSourceCursor *cursor)
{
    ma_decoder_config config = ma_decoder_config_init(SAMPLE_FORMAT, CHANNEL_COUNT, SAMPLE_RATE);

    cursor->generator = ha_alloc(sizeof(ma_decoder));
    ma_result result = ma_decoder_init_file(file_name, &config, cursor->generator);
    if (result != MA_SUCCESS) {
        printf("Failed to load %s.\n", file_name);
        return;
    }

    cursor->read = read_music_cursor_from_file;
    cursor->free = free_music_cursor_from_file;
    cursor->reset = reset_music_cursor_from_file;
}

//-----------------------------------------------------------------------------
// Music System Functions
//-----------------------------------------------------------------------------

void
regist_music_movement(u32 id, MusicMovementCallback callback)
{
    ma_mutex_lock(&music_system_main_lock);

    music_movement_table[music_movement_table_size].callback = callback;
    music_movement_table[music_movement_table_size].id = id;
    music_movement_table_size += 1;

    ma_mutex_unlock(&music_system_main_lock);
}

void
call_music_movement(u32 id)
{
    ma_mutex_lock(&music_system_main_lock);

    MusicMovementTableEntry *entry = NULL;
    for (u32 i = 0; i < music_movement_table_size; i += 1) {
        if (music_movement_table[i].id == id) {
            entry = &music_movement_table[i];
            break;
        }
    }

    if (entry == NULL) {
        printf("Você é feio\n");
        ma_mutex_unlock(&music_system_main_lock);
        return;
    }

    bool found = false;
    for (u32 i = 0; i < ACTIVE_MOVEMENTS_MAX_QNT; i += 1) {
        if (!music_active_movements[i].active) {
            music_active_movements[i].id = id;
            music_active_movements[i].callback = entry->callback;
            music_active_movements[i].step = 0;
            music_active_movements[i].active = true;
            found = true;
            break;
        }
    }

    if (!found) {
        printf("Você é feio 2\n");
        ma_mutex_unlock(&music_system_main_lock);
        return;
    }

    ma_mutex_unlock(&music_system_main_lock);
}

void
set_music_bpm(float bpm)
{
    ma_mutex_lock(&music_system_main_lock);

    float bps = bpm/60.0;
    float beat_slice_duration_seconds = (1.0/16.0)/bps;
    music_slice_duration_microseconds = (i64)(beat_slice_duration_seconds*1000.0*1000.0);
printf("microseconds = %d\n", music_slice_duration_microseconds);

    ma_mutex_unlock(&music_system_main_lock);
}

static void *
music_loop(void *arg)
{
    music_last_beat_slice_timestamp = get_micro_timestamp();

    while (true) {
        ma_mutex_lock(&music_system_main_lock);

        printf("\rbeat_slice = %d ", music_actual_beat_slice);
        fflush(stdout);

        for (u32 i = 0; i < ACTIVE_MOVEMENTS_MAX_QNT; i += 1) {
            MusicActiveMovement *it = &music_active_movements[i];
            
            if (it->active) {
                MusicMovementStatus status = it->callback(
                    music_actual_beat_slice,
                    it->step,
                    &music_main_ctx
                );

                switch (status) {
                case RUNNING:
                    it->step += 1;
                    break;
                case FINISHED:
                    it->active = false;
                    break;
                }
            }
        }

        ma_mutex_unlock(&music_system_main_lock);

        music_actual_beat_slice = (music_actual_beat_slice + 1)%16;

        // NOTE(Igor): this can generate problems, create a mutex exclusive for bpm
        while (true) {
            u64 now = get_micro_timestamp();
            if (now > (music_last_beat_slice_timestamp + music_slice_duration_microseconds)) {
                music_last_beat_slice_timestamp = get_micro_timestamp();
                break;
            }
        }
    }
}

static u32
mix_music_channel(MusicChannel *channel, f32 *output, u32 frame_count)
{
    f32 buffer[4096];

    u32 total_read = 0;
    while (total_read < frame_count) {
        u32 read_this_iteration;
        u32 total_remaining = frame_count - total_read;
        u32 to_read_this_iteration = ma_countof(buffer) / CHANNEL_COUNT;
        if (to_read_this_iteration > total_remaining) {
            to_read_this_iteration = total_remaining;
        }

        read_this_iteration = channel->cursor.read(&channel->cursor, buffer, to_read_this_iteration);
        if (read_this_iteration == 0) {
            break;
        }

        u32 i = 0;
        while (i < read_this_iteration*CHANNEL_COUNT) {
            for (u32 j = 0; j < CHANNEL_COUNT; j += 1) {
                output[total_read*CHANNEL_COUNT + i] += 
                    buffer[i]*channel->volum*channel->panning_vec[j];
                i += 1;
            }
        }

        total_read += read_this_iteration;

        if (read_this_iteration < to_read_this_iteration) {
            break;
        }
    }
    
    return total_read;
}
