#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define ha_alloc                    malloc
#define ha_free                     free
#define SAMPLE_FORMAT               ma_format_f32
#define CHANNEL_COUNT               2
#define SAMPLE_RATE                 48000
#define EFFECT_CHANNELS_QNT         32
#define MUSIC_CHANNELS_QNT          32
#define AUDIO_SOURCE_QNT            4096
#define AUDIO_SOURCE_DATA_FRAME_QNT 268435456 // 1 GB of f32 pcm frames 
#define ACTIVE_MOVEMENTS_MAX_QNT    64
#define MOVEMENT_TABLE_SIZE         512

typedef ma_int8   i8;
typedef ma_int16  i16;
typedef ma_int32  i32;
typedef ma_int64  i64;
typedef ma_uint8  u8;
typedef ma_uint16 u16;
typedef ma_uint32 u32;
typedef ma_uint64 u64;
typedef float     f32;
typedef double    f64;

//-----------------------------------------------------------------------------
// Some Useful Functions
//-----------------------------------------------------------------------------

static u8 *
str_copy(u8 *src)
{
    u32 p = 0;
    while (src[p] != '\0') {
        p += 1;
    }

    u8 *n = (u8 *)ha_alloc(sizeof(u8)*(p + 1));

    p = 0;
    while (src[p] != '\0') {
        n[p] = src[p];
        p += 1;
    }
    n[p] = '\0';

    return n;
}

static bool
str_cmp(u8 *a, u8 *b)
{
    u32 p = 0;
    while (true) {
        if (a[p] == b[p]) {
            if (a[p] == '\0') {
                return true;
            }
        } else {
            break;
        }
        p += 1;
    }

    return false;
}

static i64
get_micro_timestamp()
{
    struct timespec spec;
    timespec_get(&spec, TIME_UTC);
    return spec.tv_nsec/1000 + spec.tv_sec*1000*1000;
}

//-----------------------------------------------------------------------------
// Main Definitions
//-----------------------------------------------------------------------------

ma_device        device;
ma_device_config device_config;

//-----------------------------------------------------------------------------
// Music Source System Definitions
//-----------------------------------------------------------------------------

typedef struct MusicSourceCursor {
    u32  (*read)  (struct MusicSourceCursor *, f32 *, u32);
    void (*free)  (struct MusicSourceCursor *);
    void (*reset) (struct MusicSourceCursor *);
    void *generator;
} MusicSourceCursor;

//-----------------------------------------------------------------------------
// Music System Definitions
//-----------------------------------------------------------------------------

typedef struct {
    bool active;
    f32  volum;
    f32  panning_vec[8];
    MusicSourceCursor cursor;
} MusicChannel;

typedef enum {
    SKIP,
    RUNNING,
    FINISHED
} MusicMovementStatus;

typedef struct {
    MusicChannel *channels;
} MusicContext;

typedef MusicMovementStatus (*MusicMovementCallback) (u32, u32, MusicContext *);

typedef struct {
    bool active;
    u32  id;
    u32  step;
    MusicMovementCallback callback;
} MusicActiveMovement;

typedef struct {
    u32 id;
    MusicMovementCallback callback;
} MusicMovementTableEntry;

MusicChannel            music_channels[MUSIC_CHANNELS_QNT];
MusicActiveMovement     music_active_movements[ACTIVE_MOVEMENTS_MAX_QNT];
MusicMovementTableEntry music_movement_table[MOVEMENT_TABLE_SIZE];
u32                     music_movement_table_size = 0;
u64                     music_last_beat_slice_timestamp;
i64                     music_slice_duration_microseconds;
u32                     music_actual_beat_slice = 0;
ma_mutex                music_system_main_lock;
MusicContext            music_main_ctx;
pthread_t               music_loop_thread;

//-----------------------------------------------------------------------------
// Effect Source Storage System Definitions
//-----------------------------------------------------------------------------

typedef struct {
    u8  *name;
    f32 *data;
    u32  frame_qnt;
} EffectSource;

typedef struct {
    f32 *data;
    u32 frame_qnt;
    u32 actual_offset;
} EffectSourceCursor;

EffectSource *effect_sources = NULL;
f32          *effect_source_data = NULL;
u32           effect_source_qnt = 0;
u32           effect_source_data_used = 0;

//-----------------------------------------------------------------------------
// Effect System Definitions
//-----------------------------------------------------------------------------

typedef struct {
    bool active;
    bool loop;
    u64  play_id;
    u32  entity_id;
    f32  base_volum;
    f32  particular_volum;
    f32  panning_vec[8];
    EffectSourceCursor cursor;
} EffectChannel;

typedef struct {
    u64  play_id;    // useful to stop sounds that can or not be playing like shoot effects
    u32  channel_id; // useful to stop looping sounds like flame thrower efffects
    bool err;        // if this field is true you know that other fields in struture can be invalid
} EffectPlay;

EffectChannel effect_channels[EFFECT_CHANNELS_QNT];
ma_mutex      effect_system_lock;
u32           last_play_id = 0;
bool          is_effect_system_paused = false;