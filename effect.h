//-----------------------------------------------------------------------------
// Effect Source Storage System Functions
//-----------------------------------------------------------------------------

static EffectSource *
create_effect_source()
{
    EffectSource *a = &effect_sources[effect_source_qnt];
    effect_source_qnt += 1;
    return a;
}

static void
reset_effect_source_cursor(EffectSourceCursor *cursor)
{
    cursor->actual_offset = 0;
}

static bool
get_effect_source_cursor(u8 *file_name, EffectSourceCursor *cursor)
{
    for (u32 i = 0; i < effect_source_qnt; i += 1) {
        if (str_cmp(file_name, effect_sources[i].name)) {
            cursor->data = effect_sources[i].data;
            cursor->frame_qnt = effect_sources[i].frame_qnt;
            cursor->actual_offset = 0;
            return true;
        }
    }
    return false;
}

static u32
read_effect_source(EffectSourceCursor *cursor, f32 *output, u32 qnt)
{
    u32 read = 0;

    while (cursor->actual_offset < cursor->frame_qnt && read < qnt*CHANNEL_COUNT) {
        f32 frame = cursor->data[cursor->actual_offset];
        for (u32 i = 0; i < CHANNEL_COUNT; i += 1) {
            output[read] = frame;        
            read += 1;
        }
        cursor->actual_offset += 1;
    }

    return read/CHANNEL_COUNT;
}

void
store_effect_source(u8 *file_name)
{
    ma_decoder_config decoder_config = ma_decoder_config_init(SAMPLE_FORMAT, 1, SAMPLE_RATE);

    ma_decoder decoder;
    ma_result result = ma_decoder_init_file(file_name, &decoder_config, &decoder);
    if (result != MA_SUCCESS) {
        printf("Failed to load %s.\n", file_name);
        return;
    }

    u8 *name = str_copy(file_name);

    EffectSource *a = create_effect_source();

    a->name = str_copy(file_name);
    a->data = &effect_source_data[effect_source_data_used];
    a->frame_qnt = (u32)ma_decoder_read_pcm_frames(&decoder, a->data,
        AUDIO_SOURCE_DATA_FRAME_QNT - effect_source_data_used);

    ma_decoder_uninit(&decoder);
}

//-----------------------------------------------------------------------------
// Effect System Functions
//-----------------------------------------------------------------------------

static u32
mix_effect_channel(EffectChannel *channel, f32 *output, u32 frame_count)
{
    f32 buffer[4096];

    u32 total_read = 0;
    while (total_read < frame_count) {
        u32 read_this_iteration;
        u32 total_remaining = frame_count - total_read;
        u32 to_read_this_iteration = ma_countof(buffer) / CHANNEL_COUNT;
        if (to_read_this_iteration > total_remaining) {
            to_read_this_iteration = total_remaining;
        }

        read_this_iteration = (u32)read_effect_source(&channel->cursor, buffer, to_read_this_iteration);
        if (read_this_iteration == 0) {
            break;
        }

        u32 i = 0;
        while (i < read_this_iteration*CHANNEL_COUNT) {
            for (u32 j = 0; j < CHANNEL_COUNT; j += 1) {
                output[total_read*CHANNEL_COUNT + i] += 
                    buffer[i]*channel->base_volum*channel->particular_volum*channel->panning_vec[j];
                    //buffer[i]*channel->base_volum*channel->particular_volum;
                i += 1;
            }
        }

        total_read += read_this_iteration;

        if (read_this_iteration < to_read_this_iteration) {
            break;  /* Reached EOF. */
        }
    }
    
    return total_read;
}

EffectPlay
play_effect(u32 entity_id, char *name, bool loop, f32 base_volum, f32 particular_volum, f32 *panning_vec)
{
    ma_mutex_lock(&effect_system_lock);

    EffectPlay p;
    p.err = true;

    // finds a free channel
    EffectChannel *c = NULL;
    u32 channel_id;
    for (channel_id = 0; channel_id < EFFECT_CHANNELS_QNT; channel_id += 1) {
        if (!effect_channels[channel_id].active) {
            c = &effect_channels[channel_id];
            break;
        }
    }

    if (c == NULL) {
        printf("Channels are full!\n");
        ma_mutex_unlock(&effect_system_lock);
        return p;
    }

    last_play_id += 1;
    u64 play_id = last_play_id;


    bool result = get_effect_source_cursor(name, &c->cursor);
    if (!result) {
        printf("Failed to get %s.\n", name);
        ma_mutex_unlock(&effect_system_lock);
        return p;
    }
    for (u32 i = 0; i < CHANNEL_COUNT; i += 1) {
        c->panning_vec[i] = panning_vec[i];
    }
    c->entity_id = entity_id;
    c->particular_volum = particular_volum;
    c->loop = loop;
    c->active = true;

    // update base volum of all channel with same entity id
    for (int i = 0; i < EFFECT_CHANNELS_QNT; i += 1) {
        if (effect_channels[i].entity_id == entity_id) {
            effect_channels[i].base_volum = base_volum;
        }
    }

    ma_mutex_unlock(&effect_system_lock);

    p.play_id = play_id;
    p.channel_id = channel_id;
    p.err = false;

    return p;
}

void
update_base_volum_and_panning(u32 entity_id, f32 base_volum, f32 *panning_vec)
{
    ma_mutex_lock(&effect_system_lock);

    for (u32 i = 0; i < EFFECT_CHANNELS_QNT; i += 1) {
        if (effect_channels[i].entity_id == entity_id) {
            effect_channels[i].base_volum = base_volum;
            for (u32 j = 0; j < CHANNEL_COUNT; j += 1) {
                effect_channels[i].panning_vec[j] = panning_vec[j];
            }
        }
    }

    ma_mutex_unlock(&effect_system_lock);
}

void
stop_effect_by_channel_id(u32 channel_id)
{
    ma_mutex_lock(&effect_system_lock);

    effect_channels[channel_id].active = false;
    
    ma_mutex_unlock(&effect_system_lock);
}

void
stop_effect_by_play_id(u64 play_id)
{
    ma_mutex_lock(&effect_system_lock);
    
    for (int i = 0; i < EFFECT_CHANNELS_QNT; i += 1) {
        if (effect_channels[i].play_id == play_id) {
            effect_channels[i].active = false;
        }
    }
    
    ma_mutex_unlock(&effect_system_lock);
}

void
stop_effects_by_entity_id(u32 entity_id)
{
    ma_mutex_lock(&effect_system_lock);
    
    for (int i = 0; i < EFFECT_CHANNELS_QNT; i += 1) {
        if (effect_channels[i].entity_id == entity_id) {
            effect_channels[i].active = false;
        }
    }
    
    ma_mutex_unlock(&effect_system_lock);
}

void
pause_effect_system()
{
    ma_mutex_lock(&effect_system_lock);
    
    is_effect_system_paused = true;
    
    ma_mutex_unlock(&effect_system_lock);
}

void
unpause_effect_system()
{
    ma_mutex_lock(&effect_system_lock);
    
    is_effect_system_paused = false;
    
    ma_mutex_unlock(&effect_system_lock);
}