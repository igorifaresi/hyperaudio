#include "definitions.h"
#include "music.h"
#include "effect.h"
#include "movements.h"

static void
data_callback(ma_device* device, void *output, const void *input, u32 frame_count)
{
    MA_ASSERT(device->playback.format == SAMPLE_FORMAT);

    
    ma_mutex_lock(&music_system_main_lock);

    for (int i = 0; i < MUSIC_CHANNELS_QNT; i += 1) {
        MusicChannel *c = &music_channels[i];
        if (c->active) {
            u32 frames_read = mix_music_channel(c, output, frame_count);
            if (frames_read < frame_count) {
                c->active = false;
            }            
        }
    }

    ma_mutex_unlock(&music_system_main_lock);



    ma_mutex_lock(&effect_system_lock);

    if (is_effect_system_paused) {
        ma_mutex_unlock(&effect_system_lock);
        return;
    }

    for (int i = 0; i < EFFECT_CHANNELS_QNT; i += 1) {
        EffectChannel *c = &effect_channels[i];
        if (c->active) {
            u32 frames_read = mix_effect_channel(c, output, frame_count);
            if (frames_read < frame_count) {
                printf("LOOPING\n");
                if (c->loop) {
                    reset_effect_source_cursor(&c->cursor);
                    mix_effect_channel(
                        c,
                        (void *)(((long)output) + frames_read*sizeof(f32)),
                        frame_count - frames_read
                    );
                } else {
                    c->active = false;
                }
            }            
        }
    }

    ma_mutex_unlock(&effect_system_lock);

    (void)input;
}

bool
init_hyperaudio()
{
    for (u32 i = 0; i < MUSIC_CHANNELS_QNT; i += 1) {
        music_channels[i].active = false;
    }

    for (u32 i = 0; i < ACTIVE_MOVEMENTS_MAX_QNT; i += 1) {
        music_active_movements[i].active = false;
    }

    effect_sources = (EffectSource *)ha_alloc(sizeof(EffectSource)*AUDIO_SOURCE_QNT);
    effect_source_data = (f32 *)ha_alloc(sizeof(f32)*AUDIO_SOURCE_DATA_FRAME_QNT);

    for (int i = 0; i < EFFECT_CHANNELS_QNT; i += 1) {
        effect_channels[i].active = false;
    }

    device_config  = ma_device_config_init(ma_device_type_playback);
    device_config.playback.format   = SAMPLE_FORMAT;
    device_config.playback.channels = CHANNEL_COUNT;
    device_config.sampleRate        = SAMPLE_RATE;
    device_config.dataCallback      = data_callback;
    device_config.pUserData         = NULL;

    if (ma_device_init(NULL, &device_config, &device) != MA_SUCCESS) {
        printf("Failed to open playback device.\n");
        return false;
    }

    if (ma_device_start(&device) != MA_SUCCESS) {
        printf("Failed to start playback device.\n");
        return false;
    }

    if (ma_mutex_init(&music_system_main_lock) != MA_SUCCESS) {
        printf("Failed to create system mutex.\n");
        return false;
    }

    if (ma_mutex_init(&effect_system_lock) != MA_SUCCESS) {
        printf("Failed to create system mutex.\n");
        return false;
    }

    set_music_bpm(60);

    music_main_ctx.channels = music_channels;

    regist_all_music_movements();

    pthread_create(&music_loop_thread, NULL, music_loop, NULL);

    return true;
}

int
main()
{
    init_hyperaudio();

    //store_effect_source("musica.mp3");

    //f32 panning[2] = {0.0, 1.0};
    //EffectPlay p = play_effect(0, "musica.mp3", true, 1.0, 0.5, panning);
    
    getchar();
    getchar();

    printf("\nO player está parado no ponto incial no spawn point:\n");
    call_music_movement(0);

    //f32 new_panning[2] = {1.0, 0.0};
    //update_base_volum_and_panning(0, 1.0, new_panning);

    getchar();
    getchar();

    printf("\nO player começou a se mover:\n");
    call_music_movement(1);

    //pause_effect_system();

    getchar();
    getchar();

    printf("\nO player se afastou ainda mais do spawn point:\n");
    call_music_movement(2);
    set_music_bpm(240);

    //unpause_effect_system();


    while (1) {}

    ma_device_uninit(&device);

    return 0;
}